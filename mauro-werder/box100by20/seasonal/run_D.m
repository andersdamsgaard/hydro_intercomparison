addpath(fullfile(pwd,'..'))

if ~exist('meshnr', 'var') || isempty(meshnr)
    meshnr = 3;
end
if ~exist('resave', 'var') || isempty(resave)
    % load mat-file and resafe .nc
    resave = false;
end

dTs = [-4 -2 0 2 4]; %(0:4)-3
for i=1:length(dTs)
    if resave
        steady_out = load(['output/sqrt_seasonal',num2str(i), '_mesh', num2str(meshnr) ,'.mat']);
        shmip_save_as_netcdf(steady_out, ['outputnc/sqrt_seasonal',num2str(i), '_mesh', num2str(meshnr),'.nc'], [], true);
    else
        para = get_para_seasonal(meshnr, dTs(i));
        steady_out = run_model(para);
        %% unwrap the model output structure:
        [para, fields, phis, h_sheets, S_channels, u_beds] = unwrap_model_output_unc(steady_out);
        [pm, pn, pin, ps, pst, psp, mesh, dmesh, pp, pt, psin, pmd, psmd] = unwrap_all_para(para);
        %% save
        save_model_run(steady_out, ['sqrt_seasonal',num2str(i), '_mesh', num2str(meshnr) ,'.mat']);
        shmip_save_as_netcdf(steady_out, ['outputnc/sqrt_seasonal',num2str(i), '_mesh', num2str(meshnr),'.nc'], [], true);
    end
end
