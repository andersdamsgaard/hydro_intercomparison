#!/usr/bin/env python2
"""Plotter for the GHIP exercise, the plotter relies on the conventions as given on the comparison framework,
Available plot as of now are:
  -Sections (called with $:GHIPPloter AllSection)
  -Map Plot (called with $:GHIPPloter SynthMap)
"""

from netCDF4 import Dataset
import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from matplotlib.colors import LogNorm

# {{{ AllSections()

def AllSection():
	"""---Allsection---
	Description :
	Plot a variable function of the x coordinate for all the participating model and a given experiment.

	The experiment tag is asked later , if only the letter of the experiment is given then all the subexperiments are plotted
	"""
	#Open Figure
	fig = plt.figure()
	fig.patch.set_alpha(0.0)
	ax = fig.add_subplot(111)
	ax.set_axes(ax)
	ax.set_xlabel('Distance from front [km]', fontsize=14)
	ax.set_ylabel('Effective Pressure [MPa]', fontsize=14)
	ax.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
	ax.set_title('Comparison for run '+ expe, fontsize=18)
	#Make space for the legend
	box = ax.get_position()
	ax.set_position([box.x0, box.y0, box.width*0.85 , box.height])

	maxval = 0.0
	minval = 1.0e10

	for i,name in enumerate(names):
		for j,tag in enumerate(plotexpe):
			NCFile=prefix+name+'/'+tag+'.nc'
			try:
				DatFile	 = Dataset(NCFile, mode='r')
			except RuntimeError:
				print 'File '+ str(NCFile)+' does not exist'
				continue 
			Xs			 = 1.0e-3*DatFile.variables['xy'][0,:]
			try:
				data     = DatFile.variables[DataName][-1,:]
			except IndexError:
				data     = DatFile.variables[DataName][0,:]

			if DataName=='N':
				data=1.0e-6*data
			DatFile.close()
			plotshade=mpl.colors.colorConverter.to_rgb(plotcolor[i])
			plotshade=[max(j*0.18,x) for x in plotshade]
			minval=min(data.min(),minval)
			maxval=max(data.max(),maxval)
			ax.plot(Xs, data, linestyle='',color=plotshade,marker=plotmark,label=name+'_'+tag)
			#and place the legend outside on right
			ax.legend(loc='center left',bbox_to_anchor=(1, 0.5))
			ax.axis([Xs.min(),Xs.max(),minval,maxval])
	plt.show
	return fig

# }}}
# {{{ SynthMap()

def SynthMap():
	totplots = (len(names))*(len(plotexpe))
	fig, axar = plt.subplots(len(names), len(plotexpe), sharex=True, sharey=True, figsize=(4.5*len(plotexpe),3*len(names)))
	fig.patch.set_alpha(0.0)
	figsizing=fig.get_size_inches()
	hleft=0.03
	hright=0.01
	hspace=0.02
	hplotspace=1.-(hleft+hright+(len(plotexpe)-1.)*hspace)
	width=hplotspace/len(plotexpe)
	vtop=0.06
	vbot=0.1
	vspace=vtop
	vplotspace=1.-(vtop+vbot+(len(names)-1.)*vspace)
	height=width*figsizing[0]*hplotspace/(2.5*figsizing[1]*vplotspace)
	# gathering data limits and defining color map
	lims=np.zeros((len(plotexpe),2))
	norm = mpl.colors.Normalize(vmin=lims[0], vmax=lims[1])
	cmap = plt.cm.coolwarm#plt.cm.RdBu
	for i,name in enumerate(names):
		for j, tag in enumerate(plotexpe):
			NCFile=prefix+name+'/'+tag+'.nc'
			try:
				DatFile	 = Dataset(NCFile, mode='r')
			except RuntimeError:
				print 'File '+ str(NCFile)+' does not exist'
				axar[i,j].set_position([hleft+(hspace+width)*j,1-(vtop+height+(vspace+height)*i),width,height])
				if i==len(names)-1:
					axar[i,j].set_xlabel('X axis [m]', fontsize=14)
					if j==0:
						axar[i,j].set_ylabel('Y axis [m]', fontsize=14)
					axar[i,j].set_title('Simulation '+name+'_'+tag, fontsize=18)
				continue
			
			Xs			 = 1.0e-3*DatFile.variables['xy'][0,:]
			Ys			 = 1.0e-3*DatFile.variables['xy'][1,:]
			elts		 = DatFile.variables['cellconnect'][:].T
			try:
				data     = DatFile.variables[DataName][-1,:]
			except IndexError:
				data     = DatFile.variables[DataName][0,:]
			if DataName=='N':
				data=1.0e-6*data
			DatFile.close()

			if name=='mwer': # fixing color scale on Mauros run
				lims[j]=[np.min(data),np.max(data)]
			Xlims=[np.min(Xs),np.max(Xs)]
			Ylims=[np.min(Ys),np.max(Ys)]
			norm = mpl.colors.Normalize(vmin=lims[j][0], vmax=lims[j][1])
			cmap = plt.cm.coolwarm#plt.cm.RdBu

			#Now Plotting
			axar[i,j].set_position([hleft+(hspace+width)*j,1-(vtop+height+(vspace+height)*i),width,height])
			axar[i,j].tricontourf(Xs, Ys, elts, data, 128, cmap=cmap,vmin=lims[j][0],vmax=lims[j][1])
			axar[i,j].tricontour(Xs, Ys, elts, data, 128, cmap=cmap,vmin=lims[j][0],vmax=lims[j][1])
			axar[i,j].set_xlim(Xlims)
			axar[i,j].set_ylim(Ylims)
			axar[i,j].set_autoscale_on(False)
			if i==len(names)-1:
				axar[i,j].set_xlabel('X axis [km]', fontsize=14)
			if j==0:
				axar[i,j].set_ylabel('Y axis [km]', fontsize=14)
			axar[i,j].set_title('Simulation '+name+'_'+tag, fontsize=18)

			if name=='mwer':
				ax2 = fig.add_axes([hleft+(hspace+width)*j,0.85-(vtop+height+(vspace+height)*(len(names)-1)),width, 0.05])
				ax2.set_title('Effective Pressure [MPa]',fontsize=18,x=0.5,y=-2.5)
				cbar = mpl.colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm, orientation='horizontal')
				plt.setp(ax2.xaxis.get_majorticklabels(), rotation=45)	
				cbar.solids.set_edgecolor("face")

	#And draw
	plt.show
	return fig

# }}}
# {{{ AllEnvelopes()

def AllEnvelopes():
	"""---AllEnvelopes---
	Description :

	Needed parameters :

	The experiment tag is asked later , if only the letter of the experiment is given then all the subexperiments are plotted
	
	Example of use :
	GHIPPlotter.AllEnvelopes('N')
	"""
	#Open Figure
	fig = plt.figure(figsize=(16,12))
	fig.patch.set_alpha(0.0)
	gs = mpl.gridspec.GridSpec(4, 3)
	gs.update(left=0.07, right=0.93, top=0.95, bottom= 0.05, wspace=0.1, hspace=0.1)

	ax1 = plt.subplot(gs[0:-1,0:2])
	ay1 = plt.subplot(gs[0,-1])
	ay2 = plt.subplot(gs[1,-1])
	ay3 = plt.subplot(gs[2,-1])
	ay4 = plt.subplot(gs[3,-1])

	ax1.set_axes(ax1)
#	ax1.set_xlabel('Distance from front [m]', fontsize=14)
	ax1.set_ylabel('Effective Pressure [MPa]', fontsize=14)
	ax1.set_title('Comparison for run 4'+ expe, fontsize=18)

	ax2 = plt.subplot(gs[-1,0:2])
	ax2.set_axes(ax1)
	ax2.set_xlabel('Distance from front [m]', fontsize=14)
	ax2.set_ylabel('Effective Pressure Spread [MPa]', fontsize=14)

	Nmax      = np.NaN*np.ones((len(names),len(plotexpe)))
	NmaxPos   = np.NaN*np.ones((len(names),len(plotexpe)))
	MaxDrain  = np.NaN*np.ones((len(names),len(plotexpe)))
	MeanDrain = np.NaN*np.ones((len(names),len(plotexpe)))
	Mindata   = 1.0e10
	MinSmooth = 1.0e10
	
	for i,name in enumerate(names):
		for j,tag in enumerate(plotexpe):
			NCFile=prefix+name+'/'+tag+'.nc'
			try:
				DatFile	 = Dataset(NCFile, mode='r')
			except RuntimeError:
				print 'File '+ str(NCFile)+' does not exist'
				continue 
			Xs			 = 1.0e-3*DatFile.variables['xy'][0,:]
			try:
				data = DatFile.variables[DataName][-1,:]
			except IndexError:
				data = DatFile.variables[DataName][0,:]

			if DataName=='N':
				data=1.0e-6*data
			DatFile.close()
			Xlims=[np.min(Xs),np.max(Xs)]
			Range=Xlims[1]-Xlims[0]
			step =Range/1000.0
			window=Range/50
			valtab=np.vstack((Xs,data)).T
			sortedtab=valtab[valtab[:,0].argsort()] #sorting function of Xs
			UpEnv=np.zeros((Range/step,2))
			LowEnv=np.zeros((Range/step,2))
			MeanEnv=np.zeros((Range/step,2))

			for k,value in enumerate(np.arange(0,Range,step)):
				maxval=min(value+window/2,Xlims[1])
				minval=max(value-window/2,0)
				Slice=sortedtab[np.where(np.logical_and(sortedtab[:,0]>=minval,sortedtab[:,0]<=maxval)),1]
				sortedtab=sortedtab[np.where(sortedtab[:,0]>=minval)]
				UpEnv[k,:]=sortedtab[np.argmax(Slice),:]
				LowEnv[k,:]=sortedtab[np.argmin(Slice),:]
				MeanEnv[k,:]=np.mean(sortedtab[0:np.shape(Slice)[1]-1,:],axis=0)
				
			x = UpEnv[:,0]
			y = UpEnv[:,1]
			SmoothUp = interp1d(x,y, kind='linear')
			x = LowEnv[:,0]
			y = LowEnv[:,1]
			SmoothLow = interp1d(x,y, kind='linear')
			xx = np.linspace(max(LowEnv[:,0].min(),UpEnv[:,0].min()),min(LowEnv[:,0].max(),UpEnv[:,0].max()), 1000)
			MinSmooth=min(MinSmooth,np.amin(SmoothUp(xx)-SmoothLow(xx)))
			# {{{get stuff
			Nmax[i,j]      = np.nanmax(data)
			NmaxPos[i,j]   = Xs[np.argmax(data)]
			MaxDrain[i,j]  = np.nanmax(SmoothUp(xx)-SmoothLow(xx))
			MeanDrain[i,j] = np.mean(SmoothUp(xx)-SmoothLow(xx))
			# }}}
		
			plotshade=mpl.colors.colorConverter.to_rgb(plotcolor[i])
			plotshade=[max(0.6,x) for x in plotshade]

			if j==3:
				Mindata=min(Mindata,np.amin(data))
				ax1.set_axes(ax1)
				ax1.set_ylabel('Effective Pressure [MPa]', fontsize=14)
				ax1.set_title('Overview for run '+ tag, fontsize=18)
				ax1.axis([0, Xlims[1],Mindata, np.nanmax(Nmax[:,j])])
				ax1.plot(Xs, data,color=plotshade,marker=plotmark,linestyle='')		
				ax1.plot(xx, SmoothLow(xx),color=plotcolor[i],linewidth=1.5,label=name)
				ax1.plot(xx, SmoothUp(xx),color=plotcolor[i],linewidth=1.5)
				ax1.xaxis.set_ticklabels([])
				ax2.axis([0, Xlims[1], MinSmooth, np.nanmax(MaxDrain[:,j])])
				ax2.set_axes(ax1)
				ax2.set_xlabel('Distance from front [km]', fontsize=14)
				ax2.set_ylabel('Effective Pressure \nSpread [MPa]', fontsize=14)
				ax2.plot(xx, SmoothUp(xx)-SmoothLow(xx),color=plotcolor[i],linewidth=1.5,linestyle='-')
				ax2.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))		
				#and place the legend outside on right
				ax1.legend(loc='upper left')
				
		#And draw
		indexes=range(1,len(plotexpe)+1,1)
		ay1.plot(indexes,Nmax[i,:],color=plotcolor[i],marker=plotmark,linestyle='',markersize=15)
		ay1.plot(indexes,Nmax[i,:],linestyle='-',color=plotshade)
		ay1.yaxis.tick_right()
		ay1.yaxis.set_ticks_position('both')
		ay1.yaxis.set_label_position('right')
		ay1.xaxis.set_ticklabels([])
		ay1.axis([min(indexes), max(indexes), np.nanmin(Nmax), np.nanmax(Nmax)])
		ay1.set_ylabel('Maximum \n effective pressure', fontsize=14)

		ay2.plot(indexes,NmaxPos[i,:],color=plotcolor[i],marker=plotmark,linestyle='',markersize=15)
		ay2.plot(indexes,NmaxPos[i,:],linestyle='-',color=plotshade)
		ay2.yaxis.tick_right()
		ay2.yaxis.set_ticks_position('both')
		ay2.yaxis.set_label_position('right')
		ay2.xaxis.set_ticklabels([])
		ay2.axis([min(indexes), max(indexes), np.nanmin(NmaxPos)-1, np.nanmax(NmaxPos)+1])
		ay2.set_ylabel('Position of the maximum  \n effective pressure [km]', fontsize=14)

		ay3.plot(indexes,MaxDrain[i,:],color=plotcolor[i],marker=plotmark,linestyle='',markersize=15)
		ay3.plot(indexes,MaxDrain[i,:],linestyle='-',color=plotshade)
		ay3.yaxis.tick_right()
		ay3.yaxis.set_ticks_position('both')
		ay3.yaxis.set_label_position('right')
		ay3.xaxis.set_ticklabels([])
		ay3.axis([min(indexes), max(indexes), np.nanmin(MaxDrain), np.nanmax(MaxDrain)])
		ay3.set_ylabel('Maximum \n drainage efficiency', fontsize=14)

		ay4.plot(indexes,MeanDrain[i,:],color=plotcolor[i],marker=plotmark,linestyle='',markersize=15)
		ay4.plot(indexes,MeanDrain[i,:],linestyle='-',color=plotshade)
		ay4.set_xlabel('Index of the simulation', fontsize=14)
		ay4.yaxis.tick_right()
		ay4.yaxis.set_ticks_position('both')
		ay4.yaxis.set_label_position('right')
		ay4.axis([min(indexes), max(indexes), np.nanmin(MeanDrain), np.nanmax(MeanDrain)])
		ay4.set_ylabel('Mean \n drainage efficiency', fontsize=14)
	
	plt.show
	return fig

# }}}

if __name__=="__main__":
	PlotList={'Sections': AllSection,
						'Maps':SynthMap,
						'Enveloppes':AllEnvelopes}
	if len(sys.argv)==1: # print help if no filename plot name is given:
		print __doc__
		sys.exit(0)
		
	home = os.getenv("HOME")
	figdir=home+'/Desktop/GHIP_Fig'
	prefix=home+'/Dropbox/GHIP_Results/'

	DataName=raw_input('Give the variable you want to plot: ')
	expe=raw_input('Give the tag of the experiment you want to plot: ')

	names = ['mwer','bdef']#Add names when they become available
	plotcolor=['r','b']#Add a new plot style when you add a new name
	plotmark='.'

	if len(expe)==1:
		if expe =='A' or expe=='B':
			plotexpe=[expe+'1',expe+'2',expe+'3',expe+'4',expe+'5',expe+'6' ]
		elif expe =='C' or expe=='E' or expe=='F' or expe=='G':
			plotexpe=[expe+'1',expe+'2',expe+'3',expe+'4',expe+'5' ]
		elif expe =='D':
			plotexpe=[expe+'1',expe+'2',expe+'3',expe+'4' ]
	else:
		plotexpe=[expe]

	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
	print sys.argv[1]
	if sys.argv[1] == 'All':
		for i,plot in enumerate(PlotList.values()):
			fig=plot()
			try:
				fig.savefig(figdir+'/PDFs/Synth/'+PlotList.keys()[i]+'-All-'+DataName+'-'.join(plotexpe)+'.pdf',format='pdf')
				fig.savefig(figdir+'/PNGs/Synth/'+PlotList.keys()[i]+'-All-'+DataName+'-'.join(plotexpe)+'.png',format='png')
			except IOError:
				print 'No Figure directory existing where needed, creating it'
				print os.path.abspath(figdir)
				os.mkdir(figdir)
				os.mkdir(figdir+'/PDFs')
				os.mkdir(figdir+'/PNGs')
				os.mkdir(figdir+'/PDFs/Synth')
				os.mkdir(figdir+'/PNGs/Synth')
				fig.savefig(figdir+'/PDFs/Synth/'+PlotList.keys()[i]+'-All-'+DataName+'-'.join(plotexpe)+'.pdf',format='pdf')
				fig.savefig(figdir+'/PNGs/Synth/'+PlotList.keys()[i]+'-All-'+DataName+'-'.join(plotexpe)+'.png',format='png')
	else:
		try:
			fig=PlotList[sys.argv[1]]()
			try:
				fig.savefig(figdir+'/PDFs/Synth/'+sys.argv[1]+'-All-'+DataName+'-'.join(plotexpe)+'.pdf',format='pdf')
				fig.savefig(figdir+'/PNGs/Synth/'+sys.argv[1]+'-All-'+DataName+'-'.join(plotexpe)+'.png',format='png')
			except IOError:
				print 'No Figure directory existing where needed, creating it'
				print os.path.abspath(figdir)
				os.mkdir(figdir)
				os.mkdir(figdir+'/PDFs')
				os.mkdir(figdir+'/PNGs')
				os.mkdir(figdir+'/PDFs/Synth')
				os.mkdir(figdir+'/PNGs/Synth')
				fig.savefig(figdir+'/PDFs/Synth/'+sys.argv[1]+'-All-'+DataName+'-'.join(plotexpe)+'.pdf',format='pdf')
				fig.savefig(figdir+'/PNGs/Synth/'+sys.argv[1]+'-All-'+DataName+'-'.join(plotexpe)+'.png',format='png')

		except KeyError:

			print 'Bad keyword for the figure production, run without keyword to get the help and check the spelling.'
