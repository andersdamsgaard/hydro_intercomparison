#!/usr/bin/env python
import numpy
import sys

# read model constants
sys.path.append('../parameters/')
import physical_parameters
import other_parameters

# include geometrical functions
sys.path.append('../input_functions/topography/python/')
import sqrttopo

# import hydrology model
sys.path.append('../../granular-channel-hydro/')
import granular_channel_drainage as gcd

# initialize model
a1 = gcd.model('A1')
a1.generateVoronoiDelaunayGrid(Lx=100e3, Ly=20e3, Nx=50, Ny=10)
B, H = sqrttopo.sqrttopo(a1.grid.node_x)
a1.addScalarField('bed_elevation', B, 'm')
a1.addScalarField('ice_thickness', H, 'm')

a1.plotGrid(save=True)
a1.plotGrid(save=True, field='bed_elevation')
a1.plotGrid(save=True, field='ice_thickness')





# read mauro-werder's output from GLaDS
#glads_a3_raw = numpy.genfromtxt('../parameters/tuning_A3')

