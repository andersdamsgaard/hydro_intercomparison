#!/usr/bin/env python

import commands
import numpy
import granular_channel_drainage as gcd

# Install with `pip install netCDF4`
# Make sure netcdf is installed, e.g. `brew install netcdf`
# Documentation: http://unidata.github.io/netcdf4-python/
from netCDF4 import Dataset

def modelWrite(model):
    '''
    Write hydrology model to netCDF file following the SHMIP-standard.
    :param model: Object containing all relevant model information and output.
    :type model: granularChannelDrainage object
    '''

    if model.short_title is None:
        filename = model.title + '.nc'
    else:
        filename = model.short_title + '.nc'

    rootgrp = Dataset(filename, 'w', format='NETCDF4')

    # global attributes
    rootgrp.title = model.title
    rootgrp.meshtype = 'unstructured'
    rootgrp.dimension = model.grid.ndim
    rootgrp.channels_on_edges = 'no'
    rootgrp.institution = 'Anders Damsgaard, IGPP/SIO/UCSD'
    rootgrp.source = 'granular-channel-hydro: ' + str(gcd.__version__) + \
        'GHIP: ' + commands.getoutput('git rev-parse --verify HEAD')
    rootgrp.references = 'http://shmip.bitbucket.io'

    # define model dimensions
    nodenr = rootgrp.createDimension('nodenr', model.grid.number_of_nodes)
    cellnr = rootgrp.createDimension('cellnr', model.grid.number_of_cells)
    dim = rootgrp.createDimension('dim', model.grid.ndim)
    nodespercell = rootgrp.createDimension('nodespercell',model.mesh.elements.shape[1]) # number of nodes per element

    time         = rootgrp.createDimension('time',last) # timesteps,
    layernr      = rootgrp.createDimension('layernr',1) # number of specific variable for ineficient system
    chlayernr    = rootgrp.createDimension('chlayernr',1) # number of specific variable for efficient system

    # define variables
    nodeind    = rootgrp.createVariable('nodenr','i4',('nodenr',))
    cellind    = rootgrp.createVariable('cellnr','i4',('cellnr',))
    dims       = rootgrp.createVariable('dim','i4',('dim',))
    NpC        = rootgrp.createVariable('nodespercell','i4',('nodespercell'))
    time       = rootgrp.createVariable('time','f8',('time'))
    layers     = rootgrp.createVariable('layernr','i4',('layernr',))
    chlayers   = rootgrp.createVariable('chlayernr','i4',('chlayernr',))
    nodes      = rootgrp.createVariable('xy','f8',('dim', 'nodenr'))
    connect    = rootgrp.createVariable('cellconnect','i4',('nodespercell', 'cellnr'))
    H          = rootgrp.createVariable('H','f8',('nodenr'))
    B          = rootgrp.createVariable('B','f8',('nodenr'))
    N          = rootgrp.createVariable('N','f8',('time','nodenr'))
    hs         = rootgrp.createVariable('hs','f8',('layernr','time','nodenr'))
    Cs         = rootgrp.createVariable('Cs','f8',('chlayernr','time','nodenr'))

    time.units = 's'
    layers.layers_description = '0-Inneficient sediment layer transmitivity'
    chlayers.chlayers_description = '0-EPL thickness'
    nodes.units = 'm'
    H.units = 'm'
    B.units = 'm'
    N.units = 'Pa'
    hs.units = 'm^2s-1'
    Cs.units = 'm'

    # insert values to arrays
    nodeind[:]   = numpy.arange(0, len(nodenr), 1)
    cellind[:]   = numpy.arange(0, len(cellnr), 1)
    dims[:]      = numpy.arange(0, len(dim), 1)
    NpC[:]       = numpy.arange(0, len(nodespercell), 1)
    layers[:]    = numpy.arange(0, len(layernr), 1)
    chlayers[:]  = numpy.arange(0, len(chlayernr), 1)
    nodes[:,:]   = numpy.column_stack((model.mesh.x, model.mesh.y)).T
    connect[:,:] = model.mesh.elements.T-1
    H[:]         = model.geometry.thickness
    B[:]         = model.geometry.base
    for t in range(0,last,1):
        time[t] = model.results.TransientSolution[t].time
        N[t,:]  = model.results.TransientSolution[t].EffectivePressure.reshape(model.mesh.x.shape)
        hs[0,t,:] = model.hydrology.sediment_transmitivity
        Cs[0,t,:] = model.results.TransientSolution[t].HydrologydcEplThickness.reshape(model.mesh.x.shape)

    rootgrp.close()
