from netCDF4 import Dataset
import numpy as np
import time
from os import path, remove

# {{{ Two Layers
def netCDFTwoLay(md,filename): #specific for steady state with no time
	if path.exists(filename):
		print ('File {} allready exist'.format(filename))
		newname=raw_input('Give a new name or "delete" to replace: ')
		if newname=='delete':
			remove(filename)
		else:
			print ('New file name is {}'.format(newname))
			filename=newname

	NCData=Dataset(filename, 'w', format='NETCDF4')
	NCData.title = md.miscellaneous.name
	NCData.meshtype='unstructured'
	NCData.dimension = "2D" ;
	NCData.channels_on_edges = "no" ;
	NCData.institution = "Basile de Fleurian, " ;
	NCData.source = "ISSM:svn-dev version 19460 GHIP: 2bcaa30570eab269d64c591d71d7b211fb33487a" ;
	NCData.references = "http://shmip.bitbucket.org" ;

	last= np.size(md.results.TransientSolution)
	NumOfFields=len(md.results.TransientSolution[0].__dict__)-2 #getting number of fields from first time minus 2 to get read of logs

	if len(md.results.TransientSolution[last-1].__dict__)!=NumOfFields:
		print ('Last time is truncated for Run {}, skipping'.format(filename))
		last=last-1


	# 2. Define dimensions
	nodenr       = NCData.createDimension('nodenr',md.mesh.numberofvertices) # number of nodes
	cellnr       = NCData.createDimension('cellnr',md.mesh.numberofelements) # number of elements
	dim          = NCData.createDimension('dim',md.mesh.dimension()) # number of dimensions
	nodespercell = NCData.createDimension('nodespercell',md.mesh.elements.shape[1]) # number of nodes per element
	time         = NCData.createDimension('time',last) # timesteps,
	layernr      = NCData.createDimension('layernr',1) # number of specific variable for ineficient system
	chlayernr    = NCData.createDimension('chlayernr',1) # number of specific variable for efficient system

	# 3. define variables
	nodeind    = NCData.createVariable('nodenr','i4',('nodenr',))
	cellind    = NCData.createVariable('cellnr','i4',('cellnr',))
	dims       = NCData.createVariable('dim','i4',('dim',))
	NpC        = NCData.createVariable('nodespercell','i4',('nodespercell'))
	time       = NCData.createVariable('time','f8',('time'))
	layers     = NCData.createVariable('layernr','i4',('layernr',))
	chlayers   = NCData.createVariable('chlayernr','i4',('chlayernr',))
	nodes      = NCData.createVariable('xy','f8',('dim', 'nodenr'))
	connect    = NCData.createVariable('cellconnect','i4',('nodespercell', 'cellnr'))
	H          = NCData.createVariable('H','f8',('nodenr'))
	B          = NCData.createVariable('B','f8',('nodenr'))
	N          = NCData.createVariable('N','f8',('time','nodenr'))
	hs         = NCData.createVariable('hs','f8',('layernr','time','nodenr'))
	Cs         = NCData.createVariable('Cs','f8',('chlayernr','time','nodenr'))

	time.units = "s"
	layers.layers_description = "0-Inneficient sediment layer transmitivity" 
	chlayers.chlayers_description = "0-EPL thickness"
	nodes.units = "m"
	H.units = "m"
	B.units = "m"
	N.units = "Pa"
	hs.units = "m^2s-1"
	Cs.units = "m"

	# 4. Place data
	nodeind[:]   = np.arange( 0, len(nodenr), 1)
	cellind[:]   = np.arange( 0, len(cellnr), 1)
	dims[:]      = np.arange( 0, len(dim), 1)
	NpC[:]       = np.arange( 0, len(nodespercell), 1)
	layers[:]    = np.arange( 0, len(layernr), 1)
	chlayers[:]  = np.arange( 0, len(chlayernr), 1)
	nodes[:,:]   = np.column_stack((md.mesh.x,md.mesh.y)).T
	connect[:,:] = md.mesh.elements.T-1
	H[:]         = md.geometry.thickness
	B[:]         = md.geometry.base
	for t in range(0,last,1):
		time[t]   = md.results.TransientSolution[t].time
		N[t,:]    = md.results.TransientSolution[t].EffectivePressure.reshape(md.mesh.x.shape)
		hs[0,t,:]	= md.hydrology.sediment_transmitivity
		Cs[0,t,:]	= md.results.TransientSolution[t].HydrologydcEplThickness.reshape(md.mesh.x.shape)

	# 5. Close file
	NCData.close()
# }}}
