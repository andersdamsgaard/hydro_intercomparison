import numpy as np
from loadmodel import loadmodel
from seasonal_runoff import SeasonForcing
import organizer as org

# {{{ def InitRun(SedTrans,SedThick,EplThick,EplCol,EplKond,Leakage,Temp,dt,prefix,RunName):
def InitRun(SedTrans,SedThick,EplThick,EplCol,EplKond,Leakage,Temp,dt,prefix,RunName):
	steps=[1]

	Org = org.organizer('repository','../Models/','prefix',prefix,'steps',steps)
	
	if Org.perform(RunName):

		md=loadmodel('../Models/E1Valley.nc')

		md.settings.waitonlock=0
		md.verbose.convergence=True

		md.miscellaneous.name=prefix+RunName

		#Hydro Model Parameters
		md.hydrology.isefficientlayer=1
		md.hydrology.sedimentlimit_flag=2
		md.hydrology.rel_tol=1.0e-6
		md.hydrology.penalty_lock=10
		md.hydrology.max_iter=100
		md.hydrology.transfer_flag=1
		md.hydrology.leakage_factor=Leakage
	
		#Forcing
		md.basalforcings.groundedice_melting_rate = 7.93e-11*md.constants.yts*np.ones((md.mesh.numberofvertices))	

		#Sediment Parameters
		md.hydrology.sediment_transmitivity= SedTrans*np.ones((md.mesh.numberofvertices))	
		md.hydrology.sediment_thickness=SedThick	

		#Epl Parameters
		md.hydrology.mask_eplactive_node=np.zeros((md.mesh.numberofvertices))
		md.hydrology.epl_conductivity=EplKond
		md.hydrology.epl_initial_thickness=EplThick
		md.hydrology.epl_colapse_thickness=EplCol
		md.hydrology.eplflip_lock=20

		#Boundaries
		front=np.nonzero(md.mesh.x<=10.)[0]
		if np.size(front)<1:
			print 'WARNING, no snout given'
		md.hydrology.spcsediment_head=np.nan*np.ones((md.mesh.numberofvertices))
		md.hydrology.spcsediment_head[front]=md.geometry.base[front]
		md.hydrology.spcepl_head=np.nan*np.ones((md.mesh.numberofvertices))
		md.hydrology.spcepl_head[front]=md.geometry.base[front]

		#Initialisation
		md.initialization.sediment_head=np.zeros((md.mesh.numberofvertices))
		md.initialization.epl_thickness=EplThick*np.ones((md.mesh.numberofvertices))
		md.initialization.epl_head=np.zeros((md.mesh.numberofvertices))
		
		#Time
		md.settings.output_frequency=1000
		md.timestepping.time_step=dt
		md.timestepping.final_time=60

		return md,Org
# }}}
# {{{ def SeasonRun(Temp,dt,prefix,RunName):
def SeasonRun(Temp,dt,prefix,RunName):
	steps=[1]

	Org = org.organizer('repository','../Models/','prefix',prefix,'steps',steps)
	
	if Org.perform(RunName):

		md=loadmodel('../Models/Finit'+RunName+'nc')
		md.miscellaneous.name=prefix+RunName
	
		#Initialization
		lasttime=np.size(md.results.TransientSolution)-1
		md.initialization.sediment_head	 = md.results.TransientSolution[lasttime].SedimentHead
		md.hydrology.mask_eplactive_node = md.results.TransientSolution[lasttime].HydrologydcMaskEplactiveNode
		md.initialization.epl_thickness	 = md.results.TransientSolution[lasttime].HydrologydcEplThickness
		md.initialization.epl_head			 = md.results.TransientSolution[lasttime].EplHead

		md.results=[]

		#Forcing
		daily     = (24.0*3600.0)/31536000.
		times			=	np.arange(0,5,daily)
		secondtime=times*md.constants.yts
		md.basalforcings.groundedice_melting_rate =	SeasonForcing(md.geometry.surface,secondtime,Temp)
		md.basalforcings.groundedice_melting_rate=md.basalforcings.groundedice_melting_rate*md.constants.yts
		md.basalforcings.groundedice_melting_rate=np.append(md.basalforcings.groundedice_melting_rate,[times],axis=0)

		#Sediment Parameters
		md.hydrology.sediment_transmitivity= SedTrans*np.ones((md.mesh.numberofvertices))	
		md.hydrology.sediment_thickness=SedThick	

		#Epl Parameters
		md.hydrology.mask_eplactive_node=np.zeros((md.mesh.numberofvertices))
		md.hydrology.epl_conductivity=EplKond
		md.hydrology.epl_initial_thickness=EplThick
		md.hydrology.epl_colapse_thickness=EplCol
		md.hydrology.eplflip_lock=20

		#Boundaries
		front=np.nonzero(md.mesh.x<=10.)[0]
		if np.size(front)<1:
			print 'WARNING, no snout given'
		md.hydrology.spcsediment_head=np.nan*np.ones((md.mesh.numberofvertices))
		md.hydrology.spcsediment_head[front]=md.geometry.base[front]
		md.hydrology.spcepl_head=np.nan*np.ones((md.mesh.numberofvertices))
		md.hydrology.spcepl_head[front]=md.geometry.base[front]

		#Initialisation
		md.initialization.sediment_head=np.zeros((md.mesh.numberofvertices))
		md.initialization.epl_thickness=EplThick*np.ones((md.mesh.numberofvertices))
		md.initialization.epl_head=np.zeros((md.mesh.numberofvertices))
		
		#Time
		md.settings.output_frequency=24
		md.timestepping.time_step=dt
		md.timestepping.final_time=np.max(times)

		return md,Org
# }}}
