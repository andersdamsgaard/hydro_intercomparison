import numpy as np
from model import *
from loadmodel import *
from setmask import *
from parameterize import *
from bamg import bamg
import organizer as org


# {{{ def Mesher(prefix,Slope)
def Mesher(prefix,Slope):
	from valley import valley
	steps=[1]
	Org = org.organizer('repository','../Models/','prefix',prefix,'steps',steps)

	MeshName=prefix+'Valley'
	if Org.perform('Init'):
		md=bamg(model(),'domain','../Exp/GlacierOutline.exp','hmax',200.,'hmin',100.0)
		md=setmask(md,'','')
		md=parameterize(md,'../Param/ValleySetUp.py')

		#Geometry
		md.geometry.base,md.geometry.thickness=valley(md.mesh.x,md.mesh.y,Slope)
		md.geometry.surface=md.geometry.base+md.geometry.thickness
		md.miscellaneous.name='ValleyGeom_'+prefix

		return md,Org
# }}}
# {{{ def ValleySlope(SedTrans,SedThick,EplThick,EplCol,EplKond,Leakage,Input,dt,prefix,RunName):
def ValleySlope(SedTrans,SedThick,EplThick,EplCol,EplKond,Leakage,Input,dt,prefix,RunName):
	steps=[1]

	Org = org.organizer('repository','../Models/','prefix',prefix,'steps',steps)
	
	if Org.perform(RunName):
		
		md=loadmodel('../Models/'+prefix+'Valley.nc')
		
		md.settings.waitonlock=0
		md.verbose.convergence=True
	
		md.miscellaneous.name=prefix+RunName

		#Hydro Model Parameters
		md.hydrology.isefficientlayer=1
		md.hydrology.sedimentlimit_flag=2
		md.hydrology.rel_tol=1.0e-6
		md.hydrology.penalty_lock=10
		md.hydrology.max_iter=100
		md.hydrology.transfer_flag=1
		md.hydrology.leakage_factor=Leakage
	
		#Forcing
		md.basalforcings.groundedice_melting_rate = Input*np.ones((md.mesh.numberofvertices))
	
		#Sediment Parameters
		md.hydrology.sediment_transmitivity= SedTrans*np.ones((md.mesh.numberofvertices))	
		md.hydrology.sediment_thickness=SedThick

		#Epl Parameters
		md.hydrology.mask_eplactive_node=np.zeros((md.mesh.numberofvertices))
		md.hydrology.epl_conductivity=EplKond
		md.hydrology.epl_initial_thickness=EplThick
		md.hydrology.epl_colapse_thickness=EplCol
		md.hydrology.eplflip_lock=20
	
		#Initialisation
		md.initialization.sediment_head=np.zeros((md.mesh.numberofvertices))
		md.initialization.epl_thickness=EplThick*np.ones((md.mesh.numberofvertices))
		md.initialization.epl_head=np.zeros((md.mesh.numberofvertices))

		#Boundaries
		front=np.nonzero(md.mesh.x<=10.)[0]
		if np.size(front)<1:
			print 'WARNING, no snout given'
		md.hydrology.spcsediment_head=np.nan*np.ones((md.mesh.numberofvertices))
		md.hydrology.spcsediment_head[front]=md.geometry.base[front]
		md.hydrology.spcepl_head=np.nan*np.ones((md.mesh.numberofvertices))
		md.hydrology.spcepl_head[front]=md.geometry.base[front]
	
		#Time
		md.settings.output_frequency=1000
		md.timestepping.time_step=dt
		md.timestepping.final_time=10.0

		return md,Org
# }}}
