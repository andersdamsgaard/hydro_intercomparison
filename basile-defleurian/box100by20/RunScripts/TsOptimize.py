from netCDF4 import Dataset
import matplotlib.pyplot as plt
import numpy as np

def TsFit(order):
	rhoi=910.0
	rhow=1.0e3
	g=9.8

	FitRun	=	'/home/bfl022/Dropbox/GHIP_Results/mwer/B3.nc'
	FitData = Dataset(FitRun,mode='r')
	IniXs			= FitData.variables['xy'][0,:]
	IniFitN		= FitData.variables['N'][-1,:]
	IniThick		= FitData.variables['H'][:]
	FitData.close()
	
	Xs,ind=np.unique(IniXs,return_index=True)
	FitN=IniFitN[ind]
	Thick=IniThick[ind]
	
	Head=((rhoi*g*Thick)-FitN)/(rhow*g)
	
	GradH=[(Head[p+1]-Head[p])/(Xs[p+1]-Xs[p]) for p,pos in enumerate(Xs[:-1])]
	GradPos=[(Xs[p+1]+Xs[p])/2. for p,pos in enumerate(Xs[:-1])]
	
	GradH=np.asarray(GradH)
	GradPos=np.asarray(GradPos)

	GradH=GradH[np.where(GradH>0)]
	GradPos=GradPos[np.where(GradH>0)]

	PosClean=GradPos[abs(GradH - np.mean(GradH)) < 1.2*np.std(GradH)]
	GradClean=GradH[abs(GradH - np.mean(GradH)) < 1.2*np.std(GradH)]

	Input=[5.79e-9*(100.0e3-x) for x in PosClean]
#	Input=[1.59e-9*(100.0e3-x) for x in PosRed]

	Input=np.asarray(Input)
	
	TsFit=Input/GradClean
	Section=np.where(PosClean[np.where(PosClean<80000.)]>10000.)
	z = np.polyfit(PosClean[Section], TsFit[Section], order)
	p = np.poly1d(z)
	
	Fiter=p(PosClean)
	Fiter[np.where(PosClean<13500.)]=1.025-np.tanh((PosClean[np.where(PosClean<13500.)]+2000.)/4000.)


	# fig = plt.figure(tight_layout=True)
	# ax = fig.add_subplot(111)
	#	ax.plot(PosClean,TsFit,'b.')
	#	ax.plot(PosClean,p(PosClean),'r-')
	#	ax.plot(PosClean,Fiter,'r-')
	#	ax.plot(PosClean,1.03-np.tanh((PosClean+5000)/4000.),'k-')
	#	ax.set_ylim(0,0.1)
	# ax2=fig.add_subplot(212)
	# ax2.plot(Xs,Thick,'b.')
	# ax2.plot(Xs,Head,'r.')
	
	#And draw
	#	plt.show(block=False)
	return p
