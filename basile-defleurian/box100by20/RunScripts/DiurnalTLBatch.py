import numpy as np
from loadmodel import loadmodel
from diurnal_runoff import DiurnalForcing
import organizer as org

# {{{ RunDi(RelAmp,dt,prefix,RunName):
def RunDi(RelAmp,dt,prefix,RunName):
	steps=[1]

	Org = org.organizer('repository','../Models/','prefix',prefix,'steps',steps)
	
	if Org.perform(RunName):

		md=loadmodel('../Models/B5'+RunName+'.nc')
		md.miscellaneous.name=prefix+RunName

		#Forcing
		# stepping=300./md.constants.yts # 5 min forcing
		stepping=3600./md.constants.yts #hourly forcing
		endtime=1.0 #few days
		times=np.arange(0.,endtime,stepping)
		secondtime=times*md.constants.yts

		inputval=DiurnalForcing(secondtime,RelAmp)
		md.hydrology.basal_moulin_input=np.outer(md.hydrology.basal_moulin_input,inputval)
		md.hydrology.basal_moulin_input=np.append(md.hydrology.basal_moulin_input,[times],axis=0)
		md.basalforcings.groundedice_melting_rate = 7.93e-11*md.constants.yts*np.ones((md.mesh.numberofvertices))
	
		#initialize from results
		lasttime=np.size(md.results.TransientSolution)-1
		md.initialization.sediment_head=md.results.TransientSolution[lasttime].SedimentHead
		md.hydrology.mask_eplactive_node=md.results.TransientSolution[lasttime].HydrologydcMaskEplactiveNode
		md.initialization.epl_thickness=md.results.TransientSolution[lasttime].HydrologydcEplThickness
		md.initialization.epl_head=md.results.TransientSolution[lasttime].EplHead
		md.results=[]

		#Time
		md.settings.output_frequency=144
		md.timestepping.time_step=dt
		md.timestepping.final_time=endtime

		return md,Org
# }}}
