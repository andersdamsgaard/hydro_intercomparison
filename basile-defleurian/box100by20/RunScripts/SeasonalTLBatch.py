import numpy as np
from loadmodel import *
from seasonal_runoff import SeasonForcing
import organizer as org

# {{{ RunSe(Temp,dt,prefix,RunName):
def RunSe(Temp,dt,prefix,RunName):
	steps=[1]

	Org = org.organizer('repository','../Models/','prefix',prefix,'steps',steps)
	if Org.perform(RunName):

		LoadName='../Models/A1'+RunName+'.nc'
		#LoadName='../Models/'+prefix+'re'+RunName+'.python'
		md=loadmodel(LoadName)
		md.miscellaneous.name=prefix+RunName

		#Initialization
		lasttime=np.size(md.results.TransientSolution)-1
		md.initialization.sediment_head	 = md.results.TransientSolution[lasttime].SedimentHead
		md.hydrology.mask_eplactive_node = md.results.TransientSolution[lasttime].HydrologydcMaskEplactiveNode
		md.initialization.epl_thickness	 = md.results.TransientSolution[lasttime].HydrologydcEplThickness
		md.initialization.epl_head			 = md.results.TransientSolution[lasttime].EplHead

		md.results= []

		#Forcing
		daily     = (24.0*3600.0)/31536000.
		times			=	np.arange(0,5,daily)
		secondtime= times*31536000.
		md.basalforcings.groundedice_melting_rate=SeasonForcing(md.geometry.surface,secondtime,Temp)
		md.basalforcings.groundedice_melting_rate=md.basalforcings.groundedice_melting_rate*md.constants.yts
		md.basalforcings.groundedice_melting_rate=np.append(md.basalforcings.groundedice_melting_rate,[times],axis=0)

		#Time
		md.settings.output_frequency=1./(dt*365.)
		md.timestepping.time_step=dt
		md.timestepping.final_time=np.max(times)

		return md,Org
# }}}
